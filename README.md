Webform OpenNorth
=================
This module provides a Webform Element plugin for Webforms (>=8.x-5.x, based on Yaml forms) that allows end users to enter a Canadian postal code,
and view a list of political representatives associated with that postal code. It uses the free OpenNorth Representative API.

![webform example](https://www.drupal.org/files/webform_opennorth.gif)


Setup
-----
Enable this module, and the Webform UI module for optional admin interface.

When creating a webform, select "Postcode OpenNorth Lookup" options and configure for display.

OpenNorth
---------
[Represent API](http://dgo.to/represent), The Open North module was looked at an an option, but the port to Drupal 8 is in it's initial state.
 
Since it's currently server side only, work would have to be done to make it compatible with client side requests. 

Remaining work
--------------
- [ ] Unit test the formatting and output functions.
- [ ] Gracefully handle server-side and client-side only fields.
- [ ] Improve configuration interface and exported config yml (currently default). 
- [ ] Allow administrators to customize submit button text.
- [ ] Integrate with OpenNorth's module to help support their D8 port.
- [ ] Add descriptive error messages for the user.
- [ ] Add some nice animation for the rebuild of the results.
- [ ] Allow administrators ability to configure displayed attributes.
- [ ] Validate that representative emails can be sent to their intended recipient.
