/**
 * @file
 * Theme functions for Webform Opennorth.
 */
(function ($, Drupal) {

  // Theme function to build a basic table.
  Drupal.theme.table = function(header, rows, attributes) {
    var tableAttributes = attributes || {};

    $tableBody = $('<tbody>').append(rows.map(function(row, i) {
      return Drupal.theme('tableRow', row, '<td>', {class: (i & 1) ? 'odd' : 'even'});
    }));

    $tableHeader = $('<thead>').append(Drupal.theme('tableRow', header, '<th>'));

    return $('<table>', tableAttributes).append($tableHeader, $tableBody);
  };

  // Theme function to build a basic table row.
  Drupal.theme.tableRow = function(row, element, attributes) {
    var cellElement   = element || '<td>';
    var rowAttributes = attributes || {};

    return $('<tr>', rowAttributes).append(row.map(function(cell) {
      return $(cellElement).html(cell);
    }));
  };

})(jQuery, Drupal);
