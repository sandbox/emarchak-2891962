/**
 * @file
 * Representative API interactions for Webform Opennorth.
 */
(function ($, Drupal) {

  'use strict';

  const apiEndpoint = 'http://represent.opennorth.ca/postcodes/';

  // Initialization function.
  function init(context) {
    // If we were to hide these using FAPI, we would be unable to edit their values on the client side.
    $('input[data-webform-opennorth-name-input]', context).closest('.form-item').addClass('hidden');
    $('input[data-webform-opennorth-email-input]', context).closest('.form-item').addClass('hidden');
  }

  // Lookup the postcode.
  function lookupRepresentatives(postcode, context) {
    $.ajax({
      type: 'GET',
      dataType: 'jsonp',
      context: context,
      url: apiEndpoint + formatPostcode(postcode)
    })
      .done(function(response) {
        // @todo harden behaviour against malformed responses.
        var $table = buildSelectionTable(response.representatives_centroid);
        $(this).after($table);

        Drupal.attachBehaviors(document);
      })
      .error(function() {
        return {
          // @todo fallback gracefully.
        }
      })
  }

  // Build the selection table from the response.
  function buildSelectionTable(representatives) {
    // @todo Allow administrators ability to configure displayed attributes.
    var header = ['', Drupal.t('Name'),  Drupal.t('District'),  Drupal.t('Office'),  Drupal.t('Level'), Drupal.t('Email')];
    var rows = representatives.map(function(rep) {

      return [
        $('<input>' , {
          type: 'radio',
          'aria-label': rep.name,
          name: 'webform-opennorth-results',
          value: rep.email
        }),
        rep.name,
        rep.district_name,
        rep.elected_office,
        rep.representative_set_name,
        rep.email,
      ]
    });

    return Drupal.theme('table', header, rows, {'data-webform-opennorth-postcode-results' : ''});
  }

  // Cleanly format the postcode.
  function formatPostcode(postcode) {
    return postcode.toUpperCase().replace(/[^A-Z0-9]/g, '');
  }

  // Check to see if the post code is valid.
  function isValidPostcode(postcode) {
    return formatPostcode(postcode).match(/^[A-Z]\d[A-Z]\d[A-Z]\d/);
  }

  /**
   * Postal Code Representative Selection.
   *
   * Allows the user to select inputs from the built tables.
   */
  Drupal.behaviors.webformOpennorthPostcodeSelection = {
    attach: function (context) {
      $(context).find('[data-webform-opennorth-postcode-results]').once('webform-opennorth-postcode-selection').each(function() {
        $("input[type='radio']", this).on('change', function() {

          // Remove all selected elements.
          $(this).closest('table').find('tr').removeClass('selected');

          // Populate fields.
          if ($(this).is(':checked')) {
            $('input[data-webform-opennorth-name-input]', context).val($(this).attr('aria-label'));
            $('input[data-webform-opennorth-email-input]', context).val($(this).attr('value'));
            $(this).closest('tr').addClass('selected');
          }
        });
      });
    }
  };

  /**
   * Representative Lookup.
   *
   * Fetches and builds the results for the tables.
   */
  Drupal.behaviors.webformOpennorthPostcodeLookup = {
    attach: function (context) {
      $(context).find('[data-webform-opennorth-postcode-btn]').once('webform-opennorth-postcode-lookup').each(function() {
        init(context);

        $(this).click(function(e) {
          e.preventDefault();
          // @todo Add some nice animation for the rebuild of the results.
          $('[data-webform-opennorth-postcode-results]', context).remove();

          var $lookupInput = $('[data-webform-opennorth-postcode-input]', context);
          var postcode = $lookupInput.val();

          // If we have a valid postcode, begin building the results.
          if (isValidPostcode(postcode)) {
            $lookupInput.removeClass('error');
            lookupRepresentatives(postcode, this);
          }
          // Otherwise fallback gracefully.
          else {
            $lookupInput.addClass('error');
            // @todo Add descriptive error messages for the user.
          }
        });
      });
    }
  };

})(jQuery, Drupal);
