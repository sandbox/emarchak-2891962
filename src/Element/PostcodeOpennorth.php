<?php

namespace Drupal\webform_opennorth\Element;

use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a webform element for an OpenNorth postal code lookup element.
 *
 * @FormElement("webform_postcode_opennorth")
 */
class PostcodeOpenNorth extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public static function getCompositeElements() {
    $elements = [];

    // All webform elements use the deprecated t() method of translation.
    // Adopting for consistency.
    $elements['postcode'] = [
      '#type'  => 'textfield',
      '#title' => t('Postcode'),
      '#attributes' => [
        'data-webform-opennorth-postcode-input' => TRUE,
      ],
    ];

    $elements['representative_name'] = [
      '#type'  => 'textfield',
      '#title' => t("Your Representative's Name"),
      '#attributes' => [
        'data-webform-opennorth-name-input' => TRUE,
      ],
    ];

    $elements['representative_email'] = [
      '#type'  => 'email',
      '#title' => t("Your Representative's Email"),
      '#attributes' => [
        'data-webform-opennorth-email-input' => TRUE,
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderCompositeFormElement($element) {
    $element = parent::preRenderCompositeFormElement($element);

    // Attach the library that handles the lookup.
    // This uses the same pattern as WebformElementLocation.
    $element['#attached']['library'][] = 'webform_opennorth/representative-api';

    $element['postcode']['#attributes']['data-webform-opennorth-postcode-input'] = TRUE;

    $element['postcode-lookup'] = [
      '#type'       => 'button',
      '#value'      => t('Find My Representative'),
      '#attributes' => [
        'data-webform-key' => $element['#webform_key'],
        'data-webform-opennorth-postcode-btn' => TRUE,
      ],
    ];

    return $element;
  }

}
