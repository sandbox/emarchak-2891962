<?php

namespace Drupal\webform_opennorth\Plugin\WebformElement;

use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform_opennorth\Element\PostcodeOpenNorth;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Provides a representative element with an OpenNorth postcode lookup.
 *
 * @WebformElement(
 *   id = "webform_postcode_opennorth",
 *   label = @Translation("Postcode OpenNorth Lookup"),
 *   description = @Translation("Provides a form element to associate a post code with a representative in the OpenNorth database."),
 *   category = @Translation("Composite elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 */
class WebformPostcodeOpennorth extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function getCompositeElements() {
    $elements = PostcodeOpenNorth::getCompositeElements();
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function getInitializedCompositeElement(array &$element) {
    $form_state = new FormState();
    $form_completed = [];
    return PostcodeOpenNorth::processWebformComposite($element, $form_state, $form_completed);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // Format the configuration form..
    $form = parent::form($form, $form_state);

    // Hide the representative elements from the form.
    // @todo provide a more graceful method to handle the server-only fields.
    unset($form['composite']['elements']['representative_name']);
    unset($form['composite']['elements']['representative_email']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmission $webform_submission, array $options = []) {
    $lines = $this->getValue($element, $webform_submission, $options);
    // This is the output when viewing the submission in plain text format.
    return array_filter($lines);
  }

  /**
   * {@inheritdoc}
   */
  public function formatHtmlItem(array $element, WebformSubmission $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);

    // This is the output when viewing the submission in HTML or Table format.
    return [
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $value['representative_name'],
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $value['representative_email'],
      ],
    ];

  }

}
